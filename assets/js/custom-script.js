jQuery(function(){

    /* number format for input with class "number-check" */
    numFormat();
})

/************************ number format **************************/
/* check nubmer format on key up */
function numFormat() {
    jQuery(document).on("keyup", 'input.number-check', function (event) {
        //jQuery('input.number-check').keyup(function (event) {
        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40) return;

        // format number
        jQuery(this).val(function (index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                ;
        });
    });
}

function numberOnly(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 47 && charCode < 58)
        return true;
    return false;
};