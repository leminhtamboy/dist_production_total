export function getCheckedBoxes(checkbox_name){
    var checkboxes = document.getElementsByName(checkbox_name);
    var checkboxesChecked = [];
    for (let i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            checkboxesChecked.push(checkboxes[i].value);
        }
    }
    return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

export function validateEmail(email) {
    const preg_string = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(preg_string.test(String(email).toLowerCase())){
        return true;
    }
    return false;
}
export function validateRequire(text) {
    if(text == '' || text == undefined || text == null){
        return false;
    }
    return true;
}